<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;

class WebsocketsUpdate
{
    use Dispatchable, InteractsWithSockets;

    public string $class;

    public int $id;

    public string $payload;

    /**
     * Create a new event instance.
     *
     * @param string $class
     * @param int $id
     * @param string $payload
     *
     */
    public function __construct(string $class, int $id, string $payload)
    {
        $this->class = $class;
        $this->id = $id;
        $this->payload = $payload;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
