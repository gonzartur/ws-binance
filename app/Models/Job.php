<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use ReflectionClass;
use ReflectionException;

class Job extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'attempts',
        'reserved_at'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'payload' => 'json',
    ];


    public function getDataAttribute()
    {
        return (object) $this->payload['data'];
    }

    public function getCommandNameAttribute()
    {
        return $this->data->commandName;
    }

    public function getCommandAttribute()
    {
        return unserialize($this->data->command);
    }

    public function getPropertiesAttribute()
    {
        $command = $this->command;
        $values = (object) [];

        try {
            $reflection = new ReflectionClass($command);
        } catch (ReflectionException $e) {
            dd($e);
        }

        $properties = $reflection->getProperties();
        $commandAttributes = array_keys(get_object_vars($this->command));

        foreach ($properties as $property) {
            $propertyName = $property->getName();
            if (!in_array($propertyName, $commandAttributes)) {
                try {
                    $property = $reflection->getProperty($propertyName);
                } catch (ReflectionException $e) {
                    dd($e);
                }

                $property->setAccessible(true);
                $values->{$propertyName}  = $property->getValue($command);
            }
        }

        return $values;
    }
}
