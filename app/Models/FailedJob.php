<?php

namespace App\Models;

class FailedJob extends Job
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'failed_jobs';

    public function getShortExceptionAttribute()
    {
        $exception = $this->exception;
        return substr($exception, 0, strpos($exception, "Stack trace:"));
    }
}
