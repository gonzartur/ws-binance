<?php
namespace App\Services\Binance\Websockets;

use Amp\Loop;
use App\Events\WebsocketsUpdate;

use App\Jobs\WebsocketsSubscribeJob;
use Illuminate\Support\Facades\Artisan;

use function Amp\Websocket\Client\connect;

abstract class Stream
{
    abstract public function getName();

    protected string $baseUrl = 'wss://stream.binance.com';

    protected string $symbol = 'btcusdt';

    protected array $request;

    protected int $id;

    public function __construct() {
        $id = rand(0, 99999999);
        $this->id = $id;
    }

    public function setSymbol(string $symbol) {
        $this->symbol = strtolower($symbol);;
        return $this;
    }

    public function subscribe(): array
    {
        $handshake = $this->baseUrl . '/ws/' . $this->getName();
        $request = json_encode([
            'method' => 'SUBSCRIBE',
            'params' => [
                $this->getName(),
            ],
            'id' => $this->id
        ]);

      WebsocketsSubscribeJob::dispatch($handshake, $request, get_called_class(), $this->id)
          ->onQueue('websocket-subscription');

      return [
          $handshake,
          $request,
          get_called_class(),
          $this->id
      ];
    }

    public function getCacheKey(): string
    {
        return implode('.', [get_called_class(), $this->id]);
    }
}