<?php


namespace App\Services\Binance\Websockets;


class Trade extends Stream
{
    protected string $name;

    public int $id;

    public const REFRESH_INTERVAL = 100;

    public const COLUMNS = [
        'e' => 'Event type',
        'E' => 'Event time',
        's' => 'Symbol',
        't' => 'Trade ID',
        'p' => 'Price',
        'q' => 'Quantity',
        'b' => 'Buyer order ID',
        'a' => 'Seller order ID',
        'T' => 'Trade time',
        'm' => 'Is the buyer the market maker?',
        'M' => 'Ignore',
    ];

    public function getName(): string
    {
        return $this->symbol . '@trade';
    }
}