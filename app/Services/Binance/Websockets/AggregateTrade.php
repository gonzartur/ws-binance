<?php


namespace App\Services\Binance\Websockets;


class AggregateTrade extends Stream
{
    protected string $name;

    public int $id;

    public const REFRESH_INTERVAL = 50;

    public const COLUMNS = [
        'e' => 'Event type',
        'E' => 'Event time',
        's' => 'Symbol',
        'a' => 'Aggregate trade ID',
        'p' => 'Price',
        'q' => 'Quantity',
        'f' => 'First trade ID',
        'l' => 'Last trade ID',
        'T' => 'Trade time',
        'm' => 'Is the buyer the market maker?',
        'M' => 'Ignore',
    ];

    public function getName(): string
    {
        return $this->symbol . '@aggTrade';
    }
}