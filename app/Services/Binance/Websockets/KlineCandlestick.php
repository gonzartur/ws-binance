<?php


namespace App\Services\Binance\Websockets;


class KlineCandlestick extends Stream
{
    protected string $name;

    public int $id;

    public string $interval = '1m';

    public const REFRESH_INTERVAL = 1000;

    public const COLUMNS = [
        'e' => 'Event type',
        'E' => 'Event time',
        's' => 'Symbol',
        "k"=> [
            't' => 'Kline start time',
            'T' => 'Kline close time',
            's' => 'Symbol',
            'i' => 'Interval',
            'f' => 'First trade ID',
            'L' => 'Last trade ID',
            'o' => 'Open price',
            'c' => 'Close price',
            'h' => 'High price',
            'l' => 'Low price',
            'v' => 'Base asset volume',
            'n' => 'Number of trades',
            'x' => 'Is this kline closed?',
            'q' => 'Quote asset volume',
            'V' => 'Taker buy base asset volume',
            'Q' => 'Taker buy quote asset volume',
            'B' => 'Ignore',
        ]
    ];

    public function getName(): string
    {
        return $this->symbol . '@kline_' . $this->interval;
    }

    public function setInterval(string $interval)
    {
        $this->interval = $interval;
    }
}