<?php

namespace App\Jobs;

use Amp\Loop;
use App\Events\WebsocketsUpdate;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;

use function Amp\Websocket\Client\connect;

class WebsocketsSubscribeJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable;

    protected string $handshake;

    protected string $request;

    protected string $class;

    protected int $id;

    /**
     * Create a new job instance.
     *
     * @param string $handshake
     * @param string $request
     * @param string $class
     * @param int $id
     */
    public function __construct(string $handshake, string $request, string $class, int $id)
    {
        $this->handshake = $handshake;
        $this->request = $request;
        $this->class = $class;
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Loop::run(function () {
            $handshake = $this->handshake;
            $request = $this->request;
            $class = $this->class;
            $id = $this->id;
            $connection = yield connect($handshake);
            yield $connection->send($request);
            while ($message = yield $connection->receive()) {
                $payload = yield $message->buffer();

                WebsocketsUpdate::dispatch($class, $id, $payload);

                if ($payload === 'Goodbye!') {
                    $this->info($payload);
                    yield $connection->close();
                    break;
                }
            }
        });
    }
}
