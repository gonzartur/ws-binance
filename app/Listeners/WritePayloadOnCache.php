<?php

namespace App\Listeners;

use App\Events\WebsocketsUpdate;
use Illuminate\Support\Facades\Cache;

class WritePayloadOnCache
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param WebsocketsUpdate $event
     * @return void
     */
    public function handle(WebsocketsUpdate $event)
    {
        $key = $this->getKey($event);
        Cache::set($key, $event->payload);
    }

    public function getKey(WebsocketsUpdate $event) {
        return implode('.', [
            $event->class,
            $event->id,
        ]);
    }
}
