<?php

namespace App\Console\Commands;

use App\Models\Job;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;

class PrintWebsocketDataTable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ws:table';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $jobs = Job::whereNotNull('reserved_at')->whereQueue('websocket-subscription')->get();

        if(!count($jobs)) {
            $this->error('No current job can be monitored.');
            return 0;
        }

        $key = self::selectJob($jobs);
        [$class, $id] = explode('.', $key);
        self::loop($key, $class, $id);

        return 0;
    }

    private function loop($key, $class, $id) {
        while(true) {
            $data = Cache::get($key);
            self::printTable($data, $class);

            $interval = 100;

            if($class::REFRESH_INTERVAL !== null) {
                $interval = $class::REFRESH_INTERVAL;
            }
            usleep($interval * 1000);

        }
    }

    private function clearScreen() {
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            system('cls');
        } else {
            system('clear');
        }
    }

    private function selectJob(Collection $jobs) {
        $options = $jobs->map(function($job) {
            return implode('.', [$job->properties->class, $job->properties->id]);
        });

        return $this->choice(
            'Select a websocket to monitor',
            $options->toArray(),
            0
        );
    }

    private function printTable($data, string $class) {
        if(self::isJson($data)) {
            $decoded = json_decode($data, true);
            $combined = array_combine(Arr::flatten($class::COLUMNS), Arr::flatten($decoded));
            self::clearScreen();
            $this->table(array_keys($combined), [$combined]);

        }
    }

    private function isJson( $value ) {
        // A non-string value can never be a JSON string.
        if ( ! is_string( $value ) ) { return false; }

        // Numeric strings are always valid JSON.
        if ( is_numeric( $value ) ) { return true; }

        // Any non-numeric JSON string must be longer than 2 characters.
        if ( strlen( $value ) < 2 ) { return false; }

        // "null" is valid JSON string.
        if ( 'null' === $value ) { return true; }

        // "true" and "false" are valid JSON strings.
        if ( 'true' === $value ) { return true; }
        if ( 'false' === $value ) { return false; }

        // Any other JSON string has to be wrapped in {}, [] or "".
        if ( '{' != $value[0] && '[' != $value[0] && '"' != $value[0] ) { return false; }

        // Note the last param (1), this limits the depth to the first level.
        $json_data = json_decode( $value, null);

        // When json_decode fails, it returns NULL.
        if ( is_null( $json_data ) ) { return false; }
        return true;
    }

}
