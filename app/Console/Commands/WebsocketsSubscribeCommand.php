<?php
namespace App\Console\Commands;

use App\Jobs\WebsocketsSubscribeJob;
use Illuminate\Console\Command;

class WebsocketsSubscribeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ws:subscribe {handshake} {request} {class} {id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Connect to a websocket and fire WebsocketUpdate event when new data comes up.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $handshake = $this->argument('handshake');
        $request = $this->argument('request');
        $class = $this->argument('class');
        $id = $this->argument('id');

        WebsocketsSubscribeJob::dispatch($handshake, $request, $class, $id);
        return 0;
    }
}
