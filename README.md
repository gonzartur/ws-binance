```bash
composer install
php artisan key:generate
```

Set DATABASE, CACHE_DRIVER and QUEUE_DRIVER on .env

```bash
php artisan migrate
```

Create a job for the subscription process
```bash
php artisan tinker
namespace App\Services\Binance\Websockets
$stream = new AggregateTrade();
or $stream = new KlineCandlestick();
or $stream = new Trade();

$stream->subscribe();
```

Now process the queue to run the subscription and start receiving data
```bash
php artisan queue:work --queue websocket-subscription --timeout 0
```

If want to see the data, open another console without closing the queue worker
```bash
php artisan ws:table
```
It will ask for which one of the current running subscriptions you want to see

Change interval KlineCandlestick (default is 1m):
```bash
$stream = new App\Services\Binance\Websockets\KlineCandlestick();
$stream->setInterval('12h');
$stream->subscribe();
```

Change stream symbol (default is btcusdt):
```bash
$stream = new App\Services\Binance\Websockets\Trade();
$stream->setSybmbol('btcusdc');
$stream->subscribe();
```

Get the latest data from the cache (with a queue worker waiting for new jobs):
```bash
$stream = new App\Services\Binance\Websockets\KlineCandlestick();
$stream->subscribe();

//When the worker pick te job generated on previous code line 
$data = Illuminate\Support\Facades\Cache::get($stream->getCacheKey());
```

